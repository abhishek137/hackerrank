/* Solution to HackerRank Problem: Project Euler #34: Digit factorials
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler034
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        
        int[] fac = getDigitFactorials();
        
        int sum = 0;
        
        for(int n=10;n<N;n++){
            if(check(n,fac)){
                sum += n;
            }
        }
        
        System.out.println(sum);
    }
    
    static boolean check(int n, int[] fac){
        char[] num = String.valueOf(n).toCharArray();
        
        int sum = 0;
        
        for(int i=0;i<num.length;i++){
            sum += fac[Character.getNumericValue(num[i])];
        }
        
        if(sum%n == 0){
            return true;
        }
        
        return false;
    }
    
    static int getFactorial(int num){
        int prod = 1;
        
        for(int i=1;i<=num;i++){
            prod *= i;
        }
        
        return prod;
    }
    
    static int[] getDigitFactorials(){
        int[] data = new int[10];
        
        for(int i=0;i<10;i++){
            data[i] = getFactorial(i);
        }
        
        return data;
    }
}