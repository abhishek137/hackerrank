/* Solution to HackerRank Problem: Project Euler #37: Truncatable primes
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler037
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    static ArrayList<Integer> primes = new ArrayList<Integer>();
    static boolean[] data;

    public static void main(String[] args) {
        Scanner scan = new  Scanner(System.in);
        
        int N = scan.nextInt();
        
        getPrimes(N);
        
        System.out.println(work());
    }
    
    static int work(){
        int sum = 0;
        
        for(int i=0; i<primes.size(); i++){
            if(check(primes.get(i))){
                sum += primes.get(i);
            }
        }
        
        return sum;
    }
    
    static boolean check(int n){
        String num = ""+n;
        
        /*handles 2,3,5,7*/
        if(num.length() == 1){
            return false;
        }
        
        for(int i=1; i<num.length();i++){
            String s = num.substring(i);
            if(!data[Integer.parseInt(s)]){
                return false;
            }
        }
        
        for(int i=num.length()-1; i>0;i--){
            String s = num.substring(0,i);
            if(!data[Integer.parseInt(s)]){
                return false;
            }
        }
        
        return true;
    }
    
    static void getPrimes(int N){
        data = new boolean[N];
        
        Arrays.fill(data,true);
        
        data[0] = false;
        data[1] = false;
        
        for(int n=2; n<N; n++){
            if(data[n]){
                primes.add(n);
                
                int mul = n+n;
                
                while(mul<N){
                    data[mul] = false;
                    mul += n;
                }
            }
        }
    }
}