/* Solution to HackerRank Problem: Project Euler #35: Circular primes
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler035
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    static ArrayList<Integer> primes = new ArrayList<Integer>();
    static boolean[] data;

    public static void main(String[] args) {
        Scanner scan = new  Scanner(System.in);
        
        int N = scan.nextInt();
        
        getPrimes(N);
        
        System.out.println(work());
    }
    
    static long work(){
        long sum = 0;
        
        for(int i=0; i<primes.size(); i++){
            if(check(primes.get(i))){
                sum += primes.get(i);
            }
        }
        
        return sum;
    }
    
    static boolean check(int n){
        String num = ""+n;
        
        for(int i=1; i<num.length();i++){            
            String s = num.substring(1)+num.substring(0,1);
            
            if(!data[Integer.parseInt(s)]){
                return false;
            }
            
            num = s;
        }
        
        return true;
    }
    
    static void getPrimes(int N){
        data = new boolean[1000000];
        
        Arrays.fill(data,true);
        
        data[0] = false;
        data[1] = false;
        
        for(int n=2; n<1000000; n++){
            if(data[n]){
                if(n<N){
                    primes.add(n);
                }               
                
                int mul = n+n;
                
                while(mul<N){
                    data[mul] = false;
                    mul += n;
                }
            }
        }
    }
}