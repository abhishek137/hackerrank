/* Solution to HackerRank Problem: Project Euler #21: Amicable numbers
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler021
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan  = new Scanner(System.in);
        int T = scan.nextInt();
        
        int max = 0;
        int[] N = new int[T];
        
        for(int t=0;t<T;t++){
            N[t] = scan.nextInt();
            max = Math.max(max,N[t]);
        }        
        
        /*calculate amicable numbers till maximum of the test cases*/
        ArrayList<Integer> amicables = getAmicableNumbers(max);
        
        for(int n=0;n<N.length;n++){
            System.out.println(getSum(amicables,N[n]));
        }
    }
    
    static int getSum(ArrayList<Integer> a,int N){
        int sum  = 0;
        
        for(int i=0;i<a.size();i++){
            if(a.get(i) < N){
                sum += a.get(i);
            }
            else{
                break;
            }
        }
        
        return sum;
    }
    
    static int getSumOfFactors(int num){
        int sum = 1;
        
        for(int i=2;i<Math.sqrt(num);i++){
            if(num%i == 0){
                sum += i;
                sum += (num/i);
            }
        }
        
        return sum;
    }
    
    static ArrayList<Integer> getAmicableNumbers(int N){
        
        ArrayList<Integer> amicables = new ArrayList();
        
        for(int i=N-1;i>=0;i--){
            int other = getSumOfFactors(i);
            
            /*check (other < i) to keep other within N*/
            if(other < i && i == getSumOfFactors(other)){
                amicables.add(i);
                amicables.add(other);
            } 
        }
        
        Collections.sort(amicables);
        return amicables;
    }
}