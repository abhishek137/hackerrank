/* Solution to HackerRank Problem: Project Euler #10: Summation of primes
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler010
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int T = scan.nextInt();        
        int max = 0;
        int[] N = new int[T];
        
        for(int i=0;i<T;i++){
            N[i] = scan.nextInt();
            
            if(N[i]>max){
                max = N[i];
            }
        }
        
        /*get prime numbers till maximum of the test cases*/
        ArrayList<Integer> primes = getPrimes(max);
        Collections.sort(primes);
        
        for(int i=0;i<N.length;i++){
            System.out.println(getSum(primes,N[i]));
        }
    }
    
    static long getSum(ArrayList<Integer> primes,int N){
        long sum = 0;
        for(int i=0;i<primes.size();i++){
            if(primes.get(i)<=N){
                sum = sum + primes.get(i);
            }
            else{
                break;
            }
        }
        return sum;
    }
    
    static ArrayList<Integer> getPrimes(int N){
        boolean[] list = new boolean[N+1];
        
        /*assume all are prime*/
        Arrays.fill(list,true);
        
        ArrayList<Integer> primes = new ArrayList();
        
        /*already know 0 and 1 are not prime*/
        list[0] = false;
        list[1] = false;
        
        for(int i=0;i<list.length;i++){
            if(list[i]){
                primes.add(i);
                
                /*set multiples of this prime number as not prime*/
                int j = i+i;
                while(j<list.length){
                    list[j] = false;
                    j=j+i;
                }
            }
        }
        
        return primes;
    }
}