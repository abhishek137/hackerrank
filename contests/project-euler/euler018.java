/* Solution to HackerRank Problem: Project Euler #18: Maximum path sum I
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler018
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int T = scan.nextInt();
        
        for(int t=0;t<T;t++){
            int N = scan.nextInt();
            
            int[][] data = new int[N][N];
            
            for(int n=0;n<N;n++){
                for(int i=0;i<n+1;i++){
                    data[n][i] = scan.nextInt();
                }
            }
            
            System.out.println(getSum(0,0,data));
        }
    }
    
    static int getSum(int index, int floor, int[][] data){
        if(floor == data.length-1){
            return data[floor][index];
        }
        else{
            return data[floor][index] + Math.max(getSum(index,floor+1,data),getSum(index+1,floor+1,data));
        }
    }
}