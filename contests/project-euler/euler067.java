/* Solution to HackerRank Problem: Project Euler #67: Maximum path sum II
 * URL: https://www.hackerrank.com/contests/projecteuler/challenges/euler067
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int T = scan.nextInt();
        
        for(int t=0;t<T;t++){
            int N = scan.nextInt();
            
            int[][] data = new int[N][N];
            
            for(int n=0;n<N;n++){
                for(int i=0;i<n+1;i++){
                    data[n][i] = scan.nextInt();
                }
            }
            
            for(int i=N-1;i>=0;i--){
                for(int j=0;j<i;j++){
                    data[i-1][j] += Math.max(data[i][j],data[i][j+1]); 
                }
            }
            
            System.out.println(data[0][0]);
        }
    }
}