/* Solution to HackerRank Problem: Fibonacci Modified
 * URL: https://www.hackerrank.com/challenges/fibonacci-modified
 */ 

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int A = scan.nextInt();
        int B = scan.nextInt();
        int N = scan.nextInt();
        
        BigInteger a = BigInteger.valueOf(A);
        BigInteger b = BigInteger.valueOf(B);
        
        for(int i=3;i<=N;i++){
            BigInteger next = next(a,b);
            
            a = b;
            b = next;
        }
        
        System.out.println(b);
    }
    
    static BigInteger next(BigInteger A,BigInteger B){
        return (B.multiply(B)).add(A);
    }
}