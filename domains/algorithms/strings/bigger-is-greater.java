/* Solution to HackerRank Problem: Bigger is Greater
 * URL: https://www.hackerrank.com/challenges/bigger-is-greater
 */
 
 import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int T = in.nextInt();
        
        for(int i=0; i<T; i++){
            find(in.next());
        }
    }
    
    static void find(String s){
        
        
        char[] c = s.toCharArray();
        
        int lc = c.length;
        
        int mi = -1;
        int si = 0;
        
        boolean flag = false;
        
        loop:
        for(int i=0; i<lc;i++){
            for(int j=i+1;j<lc;j++){
                if(c[lc-1-i]>c[lc-1-j]){
                    
                    if((lc-1-j) > mi){
                        mi = lc-1-j;
                        si = lc-1-i;
                        flag = true;
                    }
                    
                    if((lc-1-i) < mi){
                        break loop;
                    }
                }
            }
        }
        
        
        if(flag){
            char t = c[mi];
            c[mi] = c[si];
            c[si] = t;
                    
            String a = new String(c);
                    
            if(lc-mi-1>1){
                char[] l = Arrays.copyOfRange(c, mi+1, lc);
                Arrays.sort(l);
                char[] f = Arrays.copyOfRange(c, 0, mi+1);

                a = new String(f)+new String(l);
            }

            System.out.println(a);
        }
        else{
            System.out.println("no answer");
        }   
    }       
}