/* Solution to HackerRank Problem: Pangrams
 * URL: https://www.hackerrank.com/challenges/pangrams
 */
 
 import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        s = s.toLowerCase();
        s = s.replaceAll("\\s+", "");
        
        char[] c = s.toCharArray();
        Arrays.sort(c);        
        
        int len = c.length;
        
        boolean flag = true;
        
        if(c[0] == 'a' && c[len-1] == 'z'){
            for(int i=0; i<len-1; i++){
                /*ascii value of two consecutive alphabets should be 0 or 1*/
                if(c[i+1]-c[i] > 1){
                    flag = false;
                    break;
                }
            }
        }
        else{
            flag = false;
        }
        
        if(flag){
           System.out.println("pangram"); 
        }
        else{
            System.out.println("not pangram"); 
        }      
    }
}