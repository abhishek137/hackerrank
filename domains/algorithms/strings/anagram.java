/* Solution to HackerRank Problem: Anagram
 * URL: https://www.hackerrank.com/challenges/anagram
 */ 
 
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int T = scan.nextInt();
        scan.nextLine();
        
        for(int t=0;t<T;t++){
            char[] data = scan.nextLine().toCharArray();
            
            /*can not be anagrams if words have different lengths*/
            if(data.length%2 != 0){
                System.out.println(-1);
            }
            else{
                int mid = data.length/2;
                
                /*split into two arrays of equal length*/
                char[] a = Arrays.copyOfRange(data,0,mid);
                char[] b = Arrays.copyOfRange(data,mid,data.length);
                
                Arrays.sort(a);
                Arrays.sort(b);
                
                ArrayList<Character> counter = new ArrayList<>();
                
                int index = 0;
                
                outerloop:
                /*search for each character from array 1 in array 2*/
                for(int i=0;i<a.length;i++){
                    for(int j=index;j<b.length;j++){
                        if(a[i] == b[j]){
                            index = j+1;
                            continue outerloop;
                        }  
                    }
                    /*if not found, count it for a replace*/
                    counter.add(a[i]);  
                }
                System.out.println(counter.size());
            }
        }
    }
}