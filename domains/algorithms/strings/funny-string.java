/* Solution to HackerRank Problem: Funny String
 * URL: https://www.hackerrank.com/challenges/funny-string
 */
 
 import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T = in.nextInt();
        
        for(int i=0; i<T; i++){
            String S = in.next();            
            
            char[] c = S.toCharArray();
            int len = c.length;
            
            boolean flag = true;
            
            for(int j=0; j<len-1; j++){
                /*no need to reverse the string*/
                if(Math.abs(c[j]-c[j+1]) != Math.abs(c[len-2-j]-c[len-1-j])){
                    flag = false;
                    break;
                }                
            }
            
            if(flag){
                System.out.println("Funny");
            }
            else{
                System.out.println("Not Funny");
            }           
        }
    }
}