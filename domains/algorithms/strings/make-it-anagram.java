/* Solution to HackerRank Problem: Make it Anagram
 * URL: https://www.hackerrank.com/challenges/make-it-anagram
 */
 
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        char[] a = in.next().toCharArray();
        char[] b = in.next().toCharArray();
        Arrays.sort(a);
        Arrays.sort(b);
        
        int i = 0;
        int j = 0;
        
        int c = 0;
        
        int la = a.length;
        int lb = b.length;
        
        /*loop will continue till end of the shorter string*/
        while(i < la && j < lb){
            if(a[i] != b[j]){
            
                if(a[i] > b[j]){
                    j++;
                }
                else{
                    i++;
                }
                
                c++;
            }else{
                i++;
                j++;
            }    
        }
        
        /*update counter to cover the extra characters in longer string*/
        if(i == la){
            c = c + lb - j; 
        }
        else if (j == lb){
            c = c + la - i;
        }
        System.out.println(c);
    }
}